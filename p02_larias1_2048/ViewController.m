//
//  ViewController.m
//  p02_larias1_2048
//
//  Created by Neo SX on 2/5/17.
//  Copyright © 2017 Neo SX. All rights reserved.
//

#import "ViewController.h"
#include <stdlib.h>

int matrix[4][4];
int overall = 0;
int req;
int Sl = 0;
int max = 0;

@interface ViewController ()
@property (assign) SystemSoundID sound;
@end

@implementation ViewController

@synthesize t00;
@synthesize t01;
@synthesize t02;
@synthesize t03;
@synthesize t10;
@synthesize t11;
@synthesize t12;
@synthesize t13;
@synthesize t20;
@synthesize t21;
@synthesize t22;
@synthesize t23;
@synthesize t30;
@synthesize t31;
@synthesize t32;
@synthesize t33;
@synthesize souls;
@synthesize lvls;
@synthesize mess;

-(IBAction)startUp{
    for(int p = 0; p < 4; p++){
        for(int n = 0; n < 4; n++){
            matrix[p][n] = 0;
        }
    }
    mess.text = @"2048 Souls Edition";
    req = 15*2;
    [self randyNum];
}

-(void)copyMatrix{
    //NSString *str = [NSString stringWithFormat:@"%d", matrix[0][0]];
    t00.text = [NSString stringWithFormat:@"%d",matrix[0][0] ];
    t01.text = [NSString stringWithFormat:@"%d",matrix[0][1] ];
    t02.text = [NSString stringWithFormat:@"%d",matrix[0][2] ];
    t03.text = [NSString stringWithFormat:@"%d",matrix[0][3] ];
    t10.text = [NSString stringWithFormat:@"%d",matrix[1][0] ];
    t11.text = [NSString stringWithFormat:@"%d",matrix[1][1] ];
    t12.text = [NSString stringWithFormat:@"%d",matrix[1][2] ];
    t13.text = [NSString stringWithFormat:@"%d",matrix[1][3] ];
    t20.text = [NSString stringWithFormat:@"%d",matrix[2][0] ];
    t21.text = [NSString stringWithFormat:@"%d",matrix[2][1] ];
    t22.text = [NSString stringWithFormat:@"%d",matrix[2][2] ];
    t23.text = [NSString stringWithFormat:@"%d",matrix[2][3] ];
    t30.text = [NSString stringWithFormat:@"%d",matrix[3][0] ];
    t31.text = [NSString stringWithFormat:@"%d",matrix[3][1] ];
    t32.text = [NSString stringWithFormat:@"%d",matrix[3][2] ];
    t33.text = [NSString stringWithFormat:@"%d",matrix[3][3] ];
    souls.text =[NSString stringWithFormat:@"%d",req ];
    lvls.text = [NSString stringWithFormat:@"%d",Sl ];
}
-(void) randyNum{
    [self calcScore];
    for(int i = 0; i < 16 ; i++){
        int rand = arc4random_uniform(16);
        int x = rand/4;
        int y = rand%4;
        if(matrix[x][y] == 0){
            int rnum = arc4random_uniform(2);
            matrix[x][y] = 2*rnum;
            i = 17;
        }
        if(matrix[x][y] != 0 && i == 15){
            mess.text = @"You Died";
            [self performSelectorOnMainThread:@selector(playDead:) withObject:NULL waitUntilDone:NO];
            self.view.backgroundColor =[UIColor grayColor];
        }
        //must do you lose if no spots open
    }
    [self copyMatrix];
   }
-(void)calcScore{
    overall = 0;
    for(int i = 0; i < 4; i++)
    {
        for(int p = 0; p < 4; p++){
            overall += matrix[i][p];
            //overall *= 3;
            //if(matrix[i][p] > max) max = matrix[i][p];
        }
        
    }
    req -= overall;
    if(req < 0){
        Sl += 1;
        req *= (-1);
    }
}

-(void)shiftL{
    for(int p = 0; p < 4; p++){
        for(int i = 0; i < 3; i++){
            if(matrix[p][i] == 0 && matrix[p][i+1] != 0){
                matrix[p][i] = matrix[p][i+1];
                matrix[p][i+1] = 0;
                i = 0;
            }
            
        }
    }
   
}

-(void)shiftR{
    for(int p = 0;p < 4; p++){
        for(int i = 3; i > 0; i--){
            if(matrix[p][i] == 0 && matrix[p][i-1] != 0){
                matrix[p][i] = matrix[p][i-1];
                matrix[p][i-1] = 0;
                i = 3;
            }
            
        }
    }
   
}

-(void)shiftD{
    for(int y = 0; y < 4;y++){
        for(int x = 3; x > 0; x--){
            if(matrix[x][y] == 0 && matrix[x -1][y] != 0){
                matrix[x][y] = matrix[x-1][y];
                matrix[x-1][y] = 0;
                x = 3;
            }
        }
    }
    

}

-(void)shiftU{
    for(int y = 0; y < 4;y++){
        for(int x = 0; x < 3; x++){
            if(matrix[x][y] == 0 && matrix[x+1][y] != 0){
                matrix[x][y] = matrix[x+1][y];
                matrix[x+1][y] = 0;
                x = 0;
            }
        }
    }
    

}

-(IBAction)moveLeft{
    [self shiftL];
    for (int c = 0; c < 4; c++) {//go through each row
       // NSString *str = NSString stringWithFormat:@"%d",matrix[0][0];
        int check = 0;
        for(int i = 0; i < 3; i++){
            if(matrix[c][i] == matrix[c][i+1] && check == 0){
                matrix[c][i] *= 2;
                matrix[c][i+1] = 0;
                check += 1;
            }
        }
    }
    [self shiftL];
    [self randyNum];
}

-(IBAction)moveRight{
    [self shiftR];
    for (int c = 0; c < 4; c++) {//go through each row
        
        int check = 0;
        for(int i = 3; i > 0; i--){
            if(matrix[c][i] == matrix[c][i-1] && check == 0){
                matrix[c][i] *= 2;
                matrix[c][i-1] = 0;
                check += 1;
            }
        }
    }
    [self shiftR];
    [self randyNum];
}

-(IBAction)moveDown{
    [self shiftD];
    for(int y = 0; y < 4; y++){// go through each column
        int check = 0;
        for(int x = 3; x > 0 ; x--){
            if(matrix[x][y] == matrix[x-1][y] && check == 0){
                matrix[x][y] *= 2;
                matrix[x-1][y] = 0;
                check += 1;
            }
        }
    }
    [self shiftD];
    [self randyNum];
}

-(IBAction)moveUp{
    [self shiftU];
    for(int y = 0; y < 4; y++){// go through each column
        int check = 0;
        for(int x = 0; x < 3; x++){
            if(matrix[x][y] == matrix[x+1][y] && check == 0){
                matrix[x][y] *= 2;
                matrix[x+1][y] = 0;
                check += 1;
            }
        }
    }
    [self shiftU];
    [self randyNum];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    NSString *soundPath = [[NSBundle mainBundle] pathForResource:@"You Died" ofType:@"aif"];
    NSURL *soundURL = [NSURL fileURLWithPath:soundPath];
    AudioServicesCreateSystemSoundID((__bridge CFURLRef)soundURL, &_sound);
    // Do any additional setup after loading the view, typically from a nib.
}
-(IBAction)playDead:(id)sender{
    AudioServicesPlaySystemSound(self.sound);
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
