//
//  ViewController.h
//  p02_larias1_2048
//
//  Created by Neo SX on 2/5/17.
//  Copyright © 2017 Neo SX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import <AudioToolbox/AudioToolbox.h>


@interface ViewController : UIViewController{
   //NSMutableArray *lists[16];
    UILabel *t00, *t01, *t02, *t03, *t10, *t11, *t12, *t13, *t20, *t21, *t22, *t23, *t30, *t31, *t32, *t33, *souls, *lvls, *mess;
}
@property NSMutableArray *lists;
@property (nonatomic, retain) IBOutlet UILabel *t00, *t01, *t02, *t03, *t10, *t11, *t12, *t13, *t20, *t21, *t22, *t23, *t30, *t31, *t32, *t33, *souls, *lvls, *mess;
-(IBAction)startUp;
-(IBAction)moveLeft;
-(IBAction)moveRight;
-(IBAction)moveUp;
-(IBAction)moveDown;
-(IBAction)playDead:(id)sender;
@end

