//
//  AppDelegate.h
//  p02_larias1_2048
//
//  Created by Neo SX on 2/5/17.
//  Copyright © 2017 Neo SX. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

